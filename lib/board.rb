class Board
  def initialize(grid=Board.default_grid)
    @grid=grid
    @ships

  end

  def self.default_grid
    i=0
    grid=[]
    while i < 10
      i2=0
      row=[]
      while i2<10
        row.push(nil)
        i2+=1
      end
      grid.push(row)
      i+=1
    end
    grid
  end

  def empty?(idx=nil)
    if idx == nil
      if self.count == 0
        return true
      end
      return false
    end
    @grid[idx[0]][idx[1]] == nil
  end

  def won?
    empty?
  end

  def full?
    if self.count == grid.map(&:length).inject(:+)
      return true
    end
    return false
  end

  def place_random_ship
    if full?
      raise "üoh oh"
    end
    while true
      pos1 = rand(0..@grid.length)
      pos2 = rand(0..@grid[0].length)
      if empty?([pos1,pos2])
        @grid[pos1][pos2]=:s
        break
      end
    end
  end



  def count
    counter=0
    @grid.each do |row|
      row.each do |value|
        if value == :s
          counter+=1
        end
      end
    end
    counter
  end


  def grid
    @grid
  end

end
